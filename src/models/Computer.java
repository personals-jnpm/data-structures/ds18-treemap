package models;

public class Computer {

    private String reference;
    private double price;
    private int gbRam;
    private int gbStorage;
    private String processor;

    public Computer(String reference, double price, int gbRam, int gbStorage, String processor) {
        this.reference = reference;
        this.price = price;
        this.gbRam = gbRam;
        this.gbStorage = gbStorage;
        this.processor = processor;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getGbRam() {
        return gbRam;
    }

    public void setGbRam(int gbRam) {
        this.gbRam = gbRam;
    }

    public int getGbStorage() {
        return gbStorage;
    }

    public void setGbStorage(int gbStorage) {
        this.gbStorage = gbStorage;
    }

    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    @Override
    public String toString() {
        return "\n \tComputador: {" +
                "\n \t \tnombre: " + reference +
                "\n \t \tprecio: " + price +
                "\n \t\tRAM: " + gbRam +
                "\n \t \talmacenamiento: " + gbStorage +
                "\n \t\tprocesador: '" + processor +
                "\n \t}";
    }
}

package models;

public class Node<E> {

    private int value;
    private String name;
    private Node<E> childLeft;
    private Node<E> childRight;

    public Node(String name) {
        this.name = name;
        childLeft = null;
        childRight = null;
    }

    public Node(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Node<E> getChildLeft() {
        return childLeft;
    }

    public void setChildLeft(Node<E> childLeft) {
        this.childLeft = childLeft;
    }

    public Node<E> getChildRight() {
        return childRight;
    }

    public void setChildRight(Node<E> childRight) {
        this.childRight = childRight;
    }

    public void insertNode(String chain) {
        if (compareMinorString(chain, name)) {
            if (childLeft != null) {
                childLeft.insertNode(chain);
            } else {
                System.out.println(" Cadena '" + chain + "' insertada a la izquierda de " + name);
                childLeft = new Node<>(chain);
            }
        } else if (compareMajorString(chain, name)) {
            if (this.childRight != null) {
                this.childRight.insertNode(chain);
            } else {
                System.out.println(" Cadena '" + chain + "' insertada a la derecha de " + name);
                childRight = new Node<>(chain);
            }
        }
    }

    public boolean compareMinorString(String chain1, String chain2){
        try {
            if (chain1.charAt(0) == chain2.charAt(0)) {
                return compareMinorString(chain1.substring(1), chain2.substring(1));
            }
            return chain1.charAt(0) < chain2.charAt(0);
        }catch (Exception e){
            System.out.println("¡Las cadenas son iguales!");
            return false;
        }
    }

    public boolean compareMajorString(String chain1, String chain2){
        try {
            if (chain1.charAt(0) == chain2.charAt(0)) {
               return compareMajorString(chain1.substring(1), chain2.substring(1));
            }
            return chain1.charAt(0) > chain2.charAt(0);
        }catch (Exception e){
            System.out.println("¡Las cadenas son iguales!");
        }
        return false;
    }

    public int size() {
        int size = 1;
        if (childLeft != null) {
            size += childLeft.size();
        }
        if (childRight != null) {
            size += childRight.size();
        }
        return size;
    }

    public int height() {
        int hl = -1;
        int hr = -1;
        if (childLeft != null) {
            hl = childLeft.height();
        }
        if (childRight != null) {
            hr = childRight.height();
        }
        return 1 + Math.max(hl, hr);
    }

    public void preorder() {
        System.out.print(name + " ");
        if (childLeft != null) {
            childLeft.preorder();
        }
        if (childRight != null) {
            childRight.preorder();
        }
    }

    public void inorder() {
        if (childLeft != null) {
            childLeft.inorder();
        }
        System.out.print(name + " ");
        if (childRight != null) {
            childRight.inorder();
        }
    }

    public void postorder() {
        if (childLeft != null) {
            childLeft.postorder();
        }
        if (childRight != null) {
            childRight.postorder();
        }
        System.out.print(name + " ");
    }

    @Override
    public String toString() {
        return "models.Node{" +
                "value=" + value +
                ", name='" + name + '\'' +
                ", childLeft=" + childLeft +
                ", childRight=" + childRight +
                '}';
    }
}

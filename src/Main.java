import models.Computer;
import models.Node;

import java.util.Iterator;
import java.util.TreeMap;

public class Main {
    public static void main(String[] args) {
        stringTree();
        treeMap();
    }

    public static void treeMap() {
        TreeMap<String, String> paisesCapitalesTM = new TreeMap<>();
        paisesCapitalesTM.put("India", "Delhi");
        paisesCapitalesTM.put("Japón", "Tokyo");
        paisesCapitalesTM.put("Francia", "Paris");
        paisesCapitalesTM.put("Colombia", "Bogotá");
        paisesCapitalesTM.put("Yugoslavia", "Belgrado");

        System.out.println();
        System.out.println("for each mostrando el mapa");
        for (String llavePais : paisesCapitalesTM.keySet()) {
            System.out.println("País:" + llavePais + " y su capital: " + paisesCapitalesTM.get(llavePais));
        }

        TreeMap<Integer, String> edadesPersonasTM = new TreeMap<>();
        edadesPersonasTM.put(23, "Juan");
        edadesPersonasTM.put(20, "Diego");
        edadesPersonasTM.put(45, "Jorge");
        edadesPersonasTM.put(2, "Gabriel");
        edadesPersonasTM.put(33, "Paola");
        edadesPersonasTM.put(55, "Blanca");
        edadesPersonasTM.put(80, "Elena");
        edadesPersonasTM.put(0, "Emilia");

        System.out.println("\nfor each mostrando el mapa");
        for (Integer llaveEdad : edadesPersonasTM.keySet()) {
            System.out.println("\t" + edadesPersonasTM.get(llaveEdad) + " tiene " + llaveEdad + " años");
        }

        TreeMap<String, Computer> computadores = new TreeMap<>();
        computadores.put("ki465", new Computer("HP pavilion gamer", 3500000, 8, 512, "Intel i5 9G"));
        computadores.put("hj736", new Computer("HP 240G7 ", 2200000, 8, 256, "Intel i5 7G"));
        computadores.put("ju574", new Computer("ACER SF313-53-76QF", 2500000, 12, 256, "Intel i5 7G"));
        computadores.put("pc5424", new Computer("ASUS ExpertBook", 3200000, 4, 512, "Amd ryzen 5"));
        computadores.put("86pñk", new Computer("HP ProDesk", 2600000, 16, 1000, "Intel i5 10G"));
        computadores.put("vy793", new Computer("ASUS TUF Gaming", 2900000, 8, 256, "Intel i7 7G"));
        computadores.put("vt137", new Computer("Lenovo IdeaPad", 2750000, 4, 512, "IAmd ryzen 5"));
        computadores.put("ño824", new Computer("HP-PB440G8", 2000000, 4, 1000, "Amd ryzen 3"));
        computadores.put("vr544", new Computer("Acer nitro", 3450000, 8, 256, "Intel i5 8G"));

        Iterator<String> keys = computadores.keySet().stream().iterator();
        System.out.println("\nComputadores [");
        while (keys.hasNext()){
            System.out.println("\t"+computadores.get(keys.next()));
        }
        System.out.println("]");
    }

    public static void stringTree() {
        Node<String> node = new Node<>("flauta");
        node.insertNode("carro");
        node.insertNode("camisa");
        node.insertNode("hormiga");
        node.insertNode("banano");
        node.insertNode("flamenco");
        node.insertNode("monitor");
        node.insertNode("escritorio");
        node.insertNode("teclado");
        node.insertNode("ratón");
        node.insertNode("kilo");
        node.insertNode("lámpara");
        node.insertNode("globo");

        System.out.print("\nRecorrer pre orden { ");
        node.preorder();
        System.out.print("} \n");

        System.out.print("\nRecorrer postor den { ");
        node.postorder();
        System.out.print("} \n");

        System.out.print("\nRecorrer inorder { ");
        node.inorder();
        System.out.print("} \n");
    }
}

